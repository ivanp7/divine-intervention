;;;; terminal-world.asd

(asdf:defsystem #:terminal-world
  :description "A world of people living on a terminal screen."
  :author "Ivan Podmazov"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:alexandria #:bordeaux-threads #:divine-intervention
               #:discrete-output-machine)
  :components ((:file "package")))

