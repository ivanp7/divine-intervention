;;;; package.lisp

(defpackage #:divine-intervention
  (:use #:cl)
  (:nicknames #:di)
  (:export :agent :make-agent :agent-id :agent-function :agent-data
           :agent-keep-running :agent-message-queue :agent-message
           :agent-running-p :agent-run :agent-wait-stop :agent-kill
           :find-running-agent
           :queue :make-queue :queue-empty-value :queue-empty-p :queue-front 
           :queue-back :queue-elements :queue-push :queue-pop :queue-consume))

