;;;; agent.lisp

(in-package #:divine-intervention)

(declaim (ftype function add-agent del-agent))

(es:define-structure agent
  (:parameters (id function &key data-plist keep-running)
   :body-macros ((alive ()
                   `(and (not (null thread))
                         (bt:thread-alive-p thread))))
   :bindings ((data (alexandria:plist-hash-table data-plist))
              (message-queue (make-queue)) thread)
   :getters (id function keep-running message-queue
             (data (key &optional default)
               (gethash key data default))
             (data-remove (key)
               (remhash key data))
             (message (&key (wait t))
               (queue-pop message-queue :wait wait))
             (running-p ()
               (alive))
             (run ()
               (unless (or (null function) (alive))
                 (setf thread (bt:make-thread 
                                (lambda ()
                                  (loop
                                    (funcall function es:self)
                                    (unless keep-running
                                      (return))))))
                 (add-agent es:self)
                 t))
             (wait-stop ()
               (when (alive)
                 (del-agent id)
                 (bt:join-thread thread)
                 (setf thread nil)))
             (kill ()
               (when (alive)
                 (del-agent id)
                 (bt:destroy-thread thread)
                 (setf thread nil))))
   :setters (keep-running
             (function ()
               (prog1 (setf function es:value)
                 (when (and function keep-running (not (alive)))
                   (funcall es:self :run))))
             (data (key)
               (setf (gethash key data) es:value))
             (message ()
               (queue-push message-queue es:value)))
   :post-forms ((when keep-running
                  (funcall es:self :run)))))

(defvar *agents* (make-hash-table))

(let ((lock (bt:make-lock))) 
  (defun add-agent (agent)
    (bt:with-lock-held (lock)
      (alexandria:if-let ((running-agent (gethash (agent-id agent) *agents*)))
        (agent-kill running-agent))
      (setf (gethash (agent-id agent) *agents*) agent)))

  (defun del-agent (id)
    (bt:with-lock-held (lock)
      (remhash id *agents*)))

  (defun find-running-agent (id)
    (bt:with-lock-held (lock)
      (gethash id *agents*))))

