# divine-intervention

What do people gain from all their labors
    at which they toil under the sun?

Generations come and generations go,
    but the earth remains forever.

The sun rises and the sun sets,
    and hurries back to where it rises.

The wind blows to the south
    and turns to the north;
round and round it goes,
    ever returning on its course.

All streams flow into the sea,
    yet the sea is never full.
To the place the streams come from,
    there they return again.

All things are wearisome,
    more than one can say.
The eye never has enough of seeing,
    nor the ear its fill of hearing.

What has been will be again,
    what has been done will be done again;
    there is nothing new under the sun.
 
Is there anything of which one can say,
    “Look! This is something new”?
It was here already, long ago;
    it was here before our time.
 
No one remembers the former generations,
    and even those yet to come
will not be remembered
    by those who follow them.

