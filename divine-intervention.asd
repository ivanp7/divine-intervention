;;;; divine-intervention.asd

(asdf:defsystem #:divine-intervention
  :description "What has been will be again,
                    what has been done will be done again;
                        there is nothing new under the sun."
  :author "Ivan Podmazov"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:alexandria #:bordeaux-threads #:cl-enhanced-structures)
  :components ((:file "package")
               (:file "queue" :depends-on ("package"))
               (:file "agent" :depends-on ("queue"))))

