;;;; queue.lisp
;;
;;;; Copyright (c) 2019 Ivan Podmazov

(in-package #:divine-intervention)

(es:define-structure queue
  (:parameters (&optional empty-value)
   :body-macros ((wait-for (value)
                   `(progn
                      (when (and wait (null front-cons))
                        (bt:condition-wait condition-variable es:lock))
                      (if (null front-cons) empty-value ,value)))
                 (pop-value ()
                   `(wait-for (prog1 (pop front-cons)
                                (when (null front-cons)
                                  (setf back-cons nil))))))
   :bindings ((condition-variable (bt:make-condition-variable))
              front-cons back-cons)
   :getters (empty-value
             (empty-p ()
               (null front-cons))
             (front (&key (wait t))
               (wait-for (car front-cons)))
             (back (&key (wait t))
               (wait-for (car back-cons)))
             (elements ()
               (copy-list front-cons))
             (push (&rest elements)
               (let ((new-last (last elements)))
                 (if (null front-cons)
                   (setf front-cons elements)
                   (setf (cdr back-cons) elements))
                 (setf back-cons new-last))
               (prog1 (if (null front-cons) empty-value (car back-cons))
                 (bt:condition-notify condition-variable)))
             (pop (&key (n 1) (wait t))
               (cond
                 ((<= n 0) empty-value)
                 ((= n 1) (pop-value))
                 (t (values-list 
                      (loop :for i :below n :collect (pop-value))))))
             (consume (consumer-fn)
               (prog1 (mapcar consumer-fn front-cons)
                 (setf front-cons nil back-cons nil))))
   :setters (empty-value)))

